package com.epam.course.renchka;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <h1>Interval of numbers.</h1>
 * Program prints odd numbers from start to the end of interval
 * and even from end to start.
 * It also prints the sum of odd and even numbers
 * @author Roman Renchka
 * @since 2019-11-09
 */
public final class IntervalOfNumbers {

  /**
   * Don't let anyone instantiate this class.
   */
  private IntervalOfNumbers() { }

  /**
   * Gets end points of interval.
   * @return array of this end points.
   * @throws IOException If an I/O error occurs.
   */
  public static int[] getEndPoints() throws IOException {
    boolean incorrectInputData = true;
    int[] endPoints = new int[2];
    BufferedReader reader =
        new BufferedReader(new InputStreamReader(System.in));
    System.out.println("Please enter the interval: [begin;end]");
    while (incorrectInputData) {
      String inputData = reader.readLine();
      inputData = inputData.replaceAll("[\\[\\]]", "");
      String[] endPointsStrFormat = inputData.split(";");
      if (endPointsStrFormat.length == 2) {
        try {
          int begin = Integer.parseInt(endPointsStrFormat[0].trim());
          int end = Integer.parseInt(endPointsStrFormat[1].trim());
          incorrectInputData = begin >= end;
          if (incorrectInputData) {
            printIncorrectInputIntervalMessage();
          } else {
            endPoints[0] = begin;
            endPoints[1] = end;
          }
        } catch (NumberFormatException e) {
          printIncorrectInputIntervalMessage();
        }
      } else {
        printIncorrectInputIntervalMessage();
      }
    }
    return endPoints;
  }

  /**
   * Gets odd numbers.
   * @param begin this is begin point of interval.
   * @param end this is end point of interval.
   * @return these numbers as List.
   */
  public static List<Integer> getOddNumbers(final int begin, final int end) {
    List<Integer> oddNumbers = new ArrayList<>();
    for (int i = begin; i <= end; i++) {
      if (i % 2 != 0) {
        oddNumbers.add(i);
      }
    }
    return oddNumbers;
  }

  /**
   * Gets reverse even numbers.
   * @param begin this is begin point of interval
   * @param end this is end point of interval
   * @return these numbers as List.
   */
  public static List<Integer> getReverseEvenNumbers(
      final int begin, final int end) {
    List<Integer> evenNumbers = new ArrayList<>();
    for (int i = begin; i <= end; i++) {
      if (i % 2 == 0) {
        evenNumbers.add(i);
      }
    }
    Collections.reverse(evenNumbers);
    return evenNumbers;
  }

  /**
   * Gets all interval numbers.
   * @param begin this is begin point of interval
   * @param end this is end point of interval
   * @return these numbers as List.
   */
  public static List<Integer> getAllNumbers(final int begin, final int end) {
    List<Integer> oddNumbers = new ArrayList<>();
    for (int i = begin; i <= end; i++) {
      oddNumbers.add(i);
    }
    return oddNumbers;
  }

  /**
   * Print message when user enter incorrect value.
   */
  private static void printIncorrectInputIntervalMessage() {
    System.out.println("Incorrect interval. Please, enter again");
  }

  /**
   * Gets sum of numbers.
   * @param numbers this array of interval numbers.
   * @return this sum.
   */
  public static int getSumOfNumbers(final List<Integer> numbers) {
    int sum = 0;
    for (int i : numbers) {
      sum += i;
    }
    return sum;
  }
}
