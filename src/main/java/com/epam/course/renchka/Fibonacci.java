package com.epam.course.renchka;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * <h1>Fibonacci.</h1>
 * The Fibonacci class prints all numbers of interval.
 * It also prints odd numbers from start to end
 * of interval and from end to start.
 * @author Roman Renchka
 * @since 2019-11-09
 */
public final class Fibonacci {

  /**
   * Value of %.
   */
  private static final int ONE_HUNDRED_PER_CENT = 100;

  /**
   * Don't let anyone instantiate this class.
   */
  private Fibonacci() { }

  /**
   * User enters some number and method gets it as size of set.
   * @return this size.
   * @throws IOException If an I/O error occurs.
   */
  public static int getSizeOfSet() throws IOException {
    BufferedReader reader =
        new BufferedReader(new InputStreamReader(System.in));
    int size = 0;
    boolean incorrectSize = true;
    while (incorrectSize) {
      try {
        size = Integer.parseInt(reader.readLine());
        incorrectSize = false;
      } catch (NumberFormatException e) {
        System.out.println("Incorrect number, please enter again:");
      }
    }
    return size;
  }

  /**
   * Builds Fibonacci numbers.
   * @param size  This is count of Fibonacci numbers
   *              which program should build.
   * @return these numbers as array.
   */
  public static int[] getNumbers(final int size) {
    if (size <= 1) {
      return size == 0 ? new int[]{0} : new int[]{0, 1};
    }
    int[] fibonacci = new int[size + 1];
    fibonacci[0] = 0;
    fibonacci[1] = 1;
    for (int i = 2; i < fibonacci.length; i++) {
      fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
    }
    return fibonacci;
  }

  /**
   * Gets biggest odd number.
   * @param array This is array of Fibonacci numbers.
   * @return this biggest number.
   */
  public static int getBiggestOddNumber(final int[] array) {
    for (int i = array.length - 1; i > -1; i--) {
      if (array[i] % 2 != 0) {
        return array[i];
      }
    }
    return -1;
  }

  /**
   * Gets biggest even number.
   * @param array This is array of Fibonacci numbers.
   * @return this biggest number.
   */
  public static int getBiggestEvenNumber(final int[] array) {
    for (int i = array.length - 1; i > -1; i--) {
      if (array[i] % 2 == 0) {
        return array[i];
      }
    }
    return 0;
  }

  /**
   * Gets percentage of Fibonacci odd numbers.
   * @param array This is array of Fibonacci numbers.
   * @return this percentage.
   */
  public static double getPercentageOfOddFibNumbers(final int[] array) {
    int count = 0;
    for (int number : array) {
      if (number % 2 != 0) {
        count++;
      }
    }
    return (count / (double) array.length) * ONE_HUNDRED_PER_CENT;
  }

  /**
   * Gets percentage of Fibonacci even numbers.
   * @param array This is array of Fibonacci numbers.
   * @return this percentage.
   */
  public static double getPercentageOfEvenFibNumbers(final int[] array) {
    int count = 0;
    for (int number : array) {
      if (number % 2 == 0) {
        count++;
      }
    }
    return (count / (double) array.length) * ONE_HUNDRED_PER_CENT;
  }
}
