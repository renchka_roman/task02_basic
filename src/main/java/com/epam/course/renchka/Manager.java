package com.epam.course.renchka;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * <h1>Manager</h1>
 * This program using Fibonacci and IntervalOfNumbers classes
 * and print their results.
 */
public final class Manager {

  /**
   * Don't let anyone instantiate this class.
   */
  private Manager() {
  }

  /**
   * This is the main method.
   *
   * @param args Unused.
   * @throws IOException If an I/O error occurs.
   */
  public static void main(String[] args) throws IOException {
    int[] endPoints = IntervalOfNumbers.getEndPoints();
    List<Integer> allNumbers =
        IntervalOfNumbers.getAllNumbers(endPoints[0], endPoints[1]);
    List<Integer> oddNumbers =
        IntervalOfNumbers.getOddNumbers(endPoints[0], endPoints[1]);
    List<Integer> evenNumbers =
        IntervalOfNumbers.getReverseEvenNumbers(endPoints[0], endPoints[1]);
    System.out.println("---------------------------------------------");
    System.out.println("RESULT:\n" + allNumbers);
    System.out.println("Odd numbers: " + oddNumbers);
    System.out.println("Reverse even numbers: " + evenNumbers);
    System.out.println("Sum of odd numbers: "
        + IntervalOfNumbers.getSumOfNumbers(oddNumbers));
    System.out.println("Sum of even numbers: "
        + IntervalOfNumbers.getSumOfNumbers(evenNumbers));

    System.out.println("---------------------------------------------");
    System.out.println("Please enter size of Fibonacci numbers set:");
    int size = Fibonacci.getSizeOfSet();
    int[] fibonacciNumbers = Fibonacci.getNumbers(size);
    System.out.println("---------------------------------------------");
    System.out.println("RESULT:\n" + Arrays.toString(fibonacciNumbers));
    System.out.println("F1 = "
        + Fibonacci.getBiggestOddNumber(fibonacciNumbers));
    System.out.println("F2 = "
        + Fibonacci.getBiggestEvenNumber(fibonacciNumbers));
    System.out.println("Percentage of odd numbers: "
        + Fibonacci.getPercentageOfOddFibNumbers(fibonacciNumbers) + " %");
    System.out.println("Percentage of even numbers: "
        + Fibonacci.getPercentageOfEvenFibNumbers(fibonacciNumbers) + " %");
  }
}
